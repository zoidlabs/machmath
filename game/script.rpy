﻿# Deklarasi karakter.
define e = Character('Eileen', color="#c8ffc8")

# Label permulaan permainan.
label start:

    e "You've created a new Ren'Py game."
    e "Once you add a story, pictures, and music, you can release it to the world!"

    return
