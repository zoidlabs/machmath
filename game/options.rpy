﻿init -1 python hide:
    config.developer = True

    config.screen_width = 800
    config.screen_height = 600

    config.window_title = u"MachMath"
    config.name = "MachMath"
    config.version = "0.0"

    theme.marker(
        widget = "#8699a7",
        widget_hover = "#9eb1ad",
        widget_text = "#dcdfd6",
        widget_selected = "#ffffff",
        disabled = "#919994",
        disabled_text = "#B6BFB9",
        label = "#ffffff",
        frame = "#6f7571",
        mm_root = "#b0b8ba",
        gm_root = "#b0b8ba",
        rounded_window = False,
        )

    # style.window.background = Frame("frame.png", 12, 12)
    # style.window.left_margin = 6
    # style.window.right_margin = 6
    # style.window.top_margin = 6
    # style.window.bottom_margin = 6
    # style.window.left_padding = 6
    # style.window.right_padding = 6
    # style.window.top_padding = 6
    # style.window.bottom_padding = 6
    # style.window.yminimum = 250


    # style.mm_menu_frame.xpos = 0.5
    # style.mm_menu_frame.xanchor = 0.5
    # style.mm_menu_frame.ypos = 0.75
    # style.mm_menu_frame.yanchor = 0.5

    # style.default.font = "DejaVuSans.ttf"
    # style.default.size = 22

    config.has_sound = True
    config.has_music = True
    config.has_voice = False

    # style.button.activate_sound = "click.wav"
    # style.imagemap.activate_sound = "click.wav"

    # config.enter_sound = "click.wav"
    # config.exit_sound = "click.wav"
    # config.sample_sound = "click.wav"
    # config.main_menu_music = "main_menu_theme.ogg"

    config.help = "README.html"

    config.enter_transition = None
    config.exit_transition = None
    config.intra_transition = None
    config.main_game_transition = None
    config.game_main_transition = None
    config.end_splash_transition = None
    config.end_game_transition = None
    config.after_load_transition = None
    config.window_show_transition = None
    config.window_hide_transition = None
    config.adv_nvl_transition = dissolve
    config.nvl_adv_transition = dissolve
    config.enter_yesno_transition = None
    config.exit_yesno_transition = None
    config.enter_replay_transition = None
    config.exit_replay_transition = None
    config.say_attribute_transition = None

python early:
    config.save_directory = "MachMath-1435147132"

init -1 python hide:
    config.default_fullscreen = False
    config.default_text_cps = 0
    config.default_afm_time = 10
